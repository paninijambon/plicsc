package plic.arbre;

import plic.arbre.expression.Expression;
import plic.exceptions.SemantiqueException;


public abstract class ArbreAbstrait {
    
	public static int cptEtiquette = 0;
	
	
    protected ArbreAbstrait() {
    }
    
   	public void incrementeCptEtiquette(){
   		Expression.cptEtiquette++;
   	}

	public abstract void verifier() throws SemantiqueException;
	
	public abstract String toString();
	
	public abstract String toMips();
    
}
