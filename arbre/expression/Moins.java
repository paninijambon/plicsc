package plic.arbre.expression;

import plic.exceptions.SemantiqueException;



public class Moins extends BinaireArithmetique {

    public Moins(Expression gauche, Expression droite, int nbLigne) {
        super(gauche, droite);
        this.ligne = nbLigne;

    }

    @Override
    public String operateur() {
        return " - ";
    }

	

	@Override
	public String toMips() {
		String sub = "";
		sub = this.gauche.toMips()+"\n" + this.droite.toMips()+ "\n" +
        "	# soustraction "+this.toString()+"\n"+
        "	add $sp,$sp,4\n"                  +	
   	   	"	lw $v0,($sp)\n"                   +
   	   	"	add $sp,$sp,4\n"                  +
   	   	"	lw $t8,($sp)\n"                   +
   	   	"	sub $v0,$t8,$v0\n"                +
        "	sw $v0,($sp)\n"                   +
        "	add $sp,$sp,-4\n";
		return sub;
	}

	@Override
	public int getValeur() {
		
		return gauche.getValeur()-droite.getValeur();
	}

	
	
    
}
