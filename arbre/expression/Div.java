package plic.arbre.expression;

import java.util.concurrent.ExecutionException;

import plic.exceptions.DivisionParZeroException;
import plic.exceptions.SemantiqueException;


public class Div extends BinaireArithmetique {

    public Div(Expression gauche, Expression droite, int nbLigne ) {
        super(gauche, droite);
        this.ligne = nbLigne;

    }

    @Override
    public String operateur() {
        return " / ";
    }

	
	public String toMips() {
		String division ="";

		if(this.droite.getValeur()==0){
			throw new SemantiqueException("Division par zero",this.getLigne());
		}
		
		division = this.gauche.toMips()+"\n" + this.droite.toMips()+ "\n" +
		           "	# divise "+this.toString()+"\n"+
		      	   "	add $sp,$sp,4\n"               +	
		      	   "	lw $v0,($sp)\n"                +
		      	   "	add $sp,$sp,4\n"               +
		      	   "	lw $t8,($sp)\n"                +
		      	   "	div $t8,$v0\n"                 +
				   "	mflo $v0\n"                    +
		           "	sw $v0,($sp)\n"                +
		           "	add $sp,$sp,-4\n";
		return division;
	}

	@Override
	public int getValeur() {
		if(this.droite.getValeur()==0){
			throw new DivisionParZeroException("Division par zero",this.getLigne());
		}
		else{
			return gauche.getValeur()/droite.getValeur();
		}
	}
    
}
