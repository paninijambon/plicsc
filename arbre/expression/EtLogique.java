package plic.arbre.expression;

import java.util.concurrent.ExecutionException;



public class EtLogique extends BinaireLogique {

    public EtLogique(Expression gauche, Expression droite, int i) {
        super(gauche, droite);
        this.type = "bool";
        this.ligne = i;
    }
    
   
    public String operateur() {
        return " et " ;
    }



	
	public String toMips() {
		String etLogique ="";
		etLogique = this.gauche.toMips()+"\n" + this.droite.toMips()+ "\n" +
		           "	# Et logique sur "+this.toString()+"\n"+
		      	   "	add $sp,$sp,4\n"  +	
		      	   "	lw $v0,($sp)\n"   +
		      	   "	add $sp,$sp,4\n"  +
		      	   "	lw $t8,($sp)\n"   +
		      	   "	and $v0,$v0,$t8\n"+ 
		           "	sw $v0,($sp)\n"   +
		           "	add $sp,$sp,-4\n";		
		return etLogique;
	}

	
	public int getValeur()  {
		return gauche.getValeur()*droite.getValeur();
	}

}
