package plic.arbre.expression;

import plic.exceptions.SemantiqueException;



public class NonLogique extends Unaire {
    
    public NonLogique(Expression expr, int i) {
        super(expr);
        this.type = "bool";
        this.ligne = i;
    }

    @Override
    public String operateur() {
        return " non " ;
    }

	@Override
	public void verifier() {
		expression.verifier();
    	if (expression.getType().equals("bool")){
    		this.setType("bool");
    	}else{
    		throw new SemantiqueException("Mauvais types, booleen attendu", this.getLigne());
    	}
		
	}

	@Override
	public String toMips() {
		String nonLogique = "";
		nonLogique = this.expression.toMips()+"\n"+
	           "	# "+this.toString()+"\n"+	
	      	   "	li $v0,1\n"                    +
	      	   "	add $sp,$sp,4\n"                   +
	      	   "	lw $t8,($sp)\n"                    +
	      	   "	sub $v0,$v0,$t8\n"                 + 
	           "	sw $v0,($sp)\n"                    +
	           "	add $sp,$sp,-4\n";
	return nonLogique;
	}

	@Override
	public int getValeur() {
		// TODO Auto-generated method stub
		return 1-expression.getValeur();
	}

}
