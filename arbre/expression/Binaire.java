package plic.arbre.expression;

import plic.exceptions.*;


public abstract class Binaire extends Expression {
    
    protected Expression gauche ;
    protected Expression droite ;

    protected Binaire(Expression gauche, Expression droite) {
        super(gauche.getLigne());
        this.gauche = gauche;
        this.droite = droite;
    }
    
    public abstract String operateur() ;

    public void verifier() throws AnalyseException{
    	if(gauche.getType() != droite.getType()){
    		throw new SemantiqueException("Types différents",this.getLigne());
    	}
    }
    @Override
    public String toString() {
        return "(" + gauche + operateur() + droite + ")" ;
    }

}
