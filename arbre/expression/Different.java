package plic.arbre.expression;

import plic.arbre.ArbreAbstrait;



public class Different extends Comparaison {

    public Different(Expression gauche, Expression droite, int i) {
        super(gauche, droite);
        this.ligne = i;
    }

    
    public String operateur() {
        return " != ";
    }

	


	
	public String toMips() {
		
		incrementeCptEtiquette();
		String egal ="";
		egal = this.gauche.toMips()+"\n" + this.droite.toMips()+ "\n" +
				  "	# Compare "+this.toString()+"\n"+
		      	   "	add $sp,$sp,4\n"            +	
		      	   "	lw $v0,($sp)\n"             +
		      	   "	add $sp,$sp,4\n"            +
		      	   "	lw $t8,($sp)\n"             +
		      	   "	beq $v0,$t8 sinon"+ArbreAbstrait.cptEtiquette+"\n" +
				   "	alors"+ArbreAbstrait.cptEtiquette+":\n"            +
		           "	li $v0, 1\n"                + 
		           "	sw $v0,0($sp)\n"            +
		           "	add $sp,$sp,-4\n"           +
		           "	j finsi"+ArbreAbstrait.cptEtiquette+"\n"           + 
		           "	sinon"+ArbreAbstrait.cptEtiquette+":\n"            + 
		           "	li $v0, 0\n"                + 
		       	   "	sw $v0,0($sp)\n"            + 
		           "	add $sp,$sp,-4\n"           +
		       	   "	finsi"+ArbreAbstrait.cptEtiquette+":\n";
		incrementeCptEtiquette();
		return egal;
	}

	
	public int getValeur() {
		if(gauche.getValeur()!= droite.getValeur()){
			return 1;
		}
		else{
			return 0;
		}
	}
  
}
