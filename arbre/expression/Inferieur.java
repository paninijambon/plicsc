package plic.arbre.expression;

import plic.arbre.ArbreAbstrait;



public class Inferieur extends Comparaison {

    public Inferieur(Expression gauche, Expression droite, int i) {
        super(gauche, droite);
        this.type = "entier";
        this.ligne = i;

    }

    @Override
    public String operateur() {
        return " < ";
    }

	

	@Override
	public String toMips() {
		incCptEtiquette();
		String inferieur ="";
		inferieur = this.gauche.toMips()+"\n" + this.droite.toMips()+ "\n" +
				   "	# Compare "+this.toString()+"\n"+
		      	   "	add $sp,$sp,4\n"         +	
		      	   "	lw $v0,($sp)\n"          +
		      	   "	add $sp,$sp,4\n"         +
		      	   "	lw $t8,($sp)\n"          +
		      	   "	sub $v0,$t8,$v0\n"       +
				   "	bgez $v0 sinon"+ArbreAbstrait.cptEtiquette+"\n" +
		           "	alors"+ArbreAbstrait.cptEtiquette+":\n"         +
		           "	li $v0, 1\n"             + 
		           "	sw $v0,0($sp)\n"         +
		           "	add $sp,$sp,-4\n"        +
		           "	j finsi"+ArbreAbstrait.cptEtiquette+"\n"        + 
		           "	sinon"+ArbreAbstrait.cptEtiquette+":\n"         + 
		           "	li $v0, 0\n"             + 
		       	   "	sw $v0,0($sp)\n"         + 
		           "	add $sp,$sp,-4\n"        +
		       	   "	finsi"+ArbreAbstrait.cptEtiquette+":\n"; 
		incCptEtiquette();
		return inferieur;
	}

	private void incCptEtiquette() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getValeur() {
		boolean res = gauche.getValeur() < droite.getValeur();
		if(res){
			return 1;
		}else{
			return 0;
		}
	}
    
}
