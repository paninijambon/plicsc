package plic.arbre.expression;

import plic.exceptions.AnalyseException;



public class ConstanteBool extends Constante {
    
	protected int value;
    public ConstanteBool(String texte, int n) {
        super(texte, n) ;
        this.type="bool";
        if(texte=="vrai"){
        	this.value=1;
        }
        else{
        	this.value=0;
        }
    }

	@Override
	public void verifier() throws AnalyseException {
		
		
	}

	@Override
	public String toMips() {
	int tmp ;
		if(this.cste=="true"){
			tmp=1;
		}
		else{
			tmp=0;
		}
		StringBuilder bool = new StringBuilder();
		bool.append("	# Range "+ this.cste+ " ("+tmp+")" +" dans $v0 et l'empile\n"+
						  "	li $v0, " + tmp + "\n" +
						  "	sw $v0,($sp) \n" +
						  "	add $sp ,$sp,-4 \n");
		return bool.toString();	
	}

	@Override
	public int getValeur() {
		// TODO Auto-generated method stub
		return this.value;
	}

	
	

}
