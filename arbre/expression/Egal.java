package plic.arbre.expression;

import plic.arbre.ArbreAbstrait;


public class Egal extends Comparaison {

    public Egal(Expression gauche, Expression droite, int i) {
        super(gauche, droite);
        this.ligne = i;
       
    }
    
    @Override
    public String operateur() {
        return " == ";
    }

	
	@Override
	public String toMips() {
		incrementeCptEtiquette();
		String egal ="";
		egal = this.gauche.toMips()+"\n" + this.droite.toMips()+ "\n" +
				  "	# Compare "+this.toString()+"\n"+
		      	   "	add $sp,$sp,4\n"            +	
		      	   "	lw $v0,($sp)\n"             +
		      	   "	add $sp,$sp,4\n"            +
		      	   "	lw $t8,($sp)\n"             +
		      	   "	bne $v0,$t8 sinon"+ArbreAbstrait.cptEtiquette+"\n" +
				   "	alors"+ArbreAbstrait.cptEtiquette+":\n"            +
		           "	li $v0, 1\n"                + 
		           "	sw $v0,0($sp)\n"            +
		           "	add $sp,$sp,-4\n"           +
		           "	j finsi"+ArbreAbstrait.cptEtiquette+"\n"           + 
		           "	sinon"+ArbreAbstrait.cptEtiquette+":\n"            + 
		           "	li $v0, 0\n"                + 
		       	   "	sw $v0,0($sp)\n"            + 
		           "	add $sp,$sp,-4\n"           +
		       	   "	finsi"+ArbreAbstrait.cptEtiquette+":\n";
		incrementeCptEtiquette();
		return egal;
	}

	@Override
	public int getValeur() {
		boolean res = gauche.getValeur() == droite.getValeur();
		if(res){
			return 1;
		}
		else{
			return 0;
		}
		
	}
    
}
