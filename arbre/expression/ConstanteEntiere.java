package plic.arbre.expression;

import plic.exceptions.AnalyseException;


public class ConstanteEntiere extends Constante {
    
		protected int valeur;
    public ConstanteEntiere(String texte, int n) {
        super(texte, n) ;
        this.type="entier";
        this.valeur=Integer.parseInt(texte);
    }

	@Override
	public void verifier() throws AnalyseException{
		
	}
	
	public int getValeur(){
		return this.valeur;
	}

	@Override
	public String toMips() {
		StringBuilder entier = new StringBuilder();
		entier.append("	# Range "+ this.cste +" dans $v0 et l'empile\n"+
						  "	li $v0, " + this.cste + "\n" +
						  "	sw $v0,($sp) \n" +
						  "	add $sp ,$sp,-4 \n");
		return entier.toString();
	}

}
