package plic.arbre.expression;


public class OuLogique extends BinaireLogique {

    public OuLogique(Expression gauche, Expression droite, int i) {
        super(gauche, droite);
        this.type = "bool";
        this.ligne = i;
    }
    
    @Override
    public String operateur() {
        return " ou " ;
    }

	

	@Override
	public String toMips() {
		
		String ou = "";
		ou = this.gauche.toMips()+"\n" + this.droite.toMips()+ "\n" +
	           "	# Ou logique sur "+this.toString()+" et empile\n"+
	      	   "	add $sp,$sp,4\n"                   +	
	      	   "	lw $v0,($sp)\n"                    +
	      	   "	add $sp,$sp,4\n"                   +
	      	   "	lw $t8,($sp)\n"                    +
	      	   "	or $v0,$v0,$t8\n"                  + 
	           "	sw $v0,($sp)\n"                    +
	           "	add $sp,$sp,-4\n";
		return ou;
	}

	@Override
	public int getValeur() {
		
		if(gauche.getValeur()+droite.getValeur() >0){
			return 1;
		}
		else{
			return 0;
		}
	}

}
