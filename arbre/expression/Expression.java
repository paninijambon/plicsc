package plic.arbre.expression;

import java.util.concurrent.ExecutionException;

import plic.arbre.ArbreAbstrait;
import plic.exceptions.AnalyseException;
import plic.exceptions.SemantiqueException;



public abstract class Expression extends ArbreAbstrait {
    
	
	 protected String type ;
	 protected int ligne;
	    
    protected Expression(int n) {
        super() ;
    }
   
    public String getType(){
    	return type;
    }
    
    public abstract int getValeur() ;
    public int getLigne(){
    	return ligne;
    }
    
    public void setLigne(int i){
    	this.ligne =i ;
    }
    
    public abstract void verifier() throws AnalyseException;
    public void setType(String str){
    	this.type = str;
    }
}
