package plic.arbre.tds;

import java.util.HashMap;

import plic.arbre.expression.Expression;
import plic.exceptions.DoubleDeclarationException;
import plic.exceptions.PasDeDeclarationException;

public class Tds {
	
	private HashMap<Entree, Symbole> tds;
	private HashMap<Entree, Expression> hmap;
	private int deplacement;
	
	private Tds(){
		tds =  new HashMap<Entree, Symbole>();
		hmap =  new HashMap<Entree, Expression>();
	}
	
	private static Tds instance = new Tds();
	
	public static Tds getInstance(){
		return instance ;
	}
	
	public HashMap<Entree, Symbole> getTds() {
		return tds;
	}

	public HashMap<Entree, Expression> getHmap() {
		return hmap;
	}


	public void ajouter(Entree entree, Symbole s) throws DoubleDeclarationException{
		
		if (tds.containsKey(entree)){
			throw new DoubleDeclarationException(entree.getEntree() +" est deja declare", entree.getLigne());
		}else{
			 s.setDepl( - (tds.size() * 4) );
		        tds.put(entree, s);
		}
	}
	
	public Symbole identifier(Entree entree,int nbligne) throws PasDeDeclarationException{
		Symbole s = tds.get(entree);
		if(s==null){
			throw new PasDeDeclarationException(entree.getEntree() +" n'a pas ete declare!", nbligne);
		}
		return s;
	}
	
	public void verifier(Entree entree, Expression e) throws PasDeDeclarationException{
		Symbole s = tds.get(entree);
		if(s==null){
			if(e == null){
				hmap.put(entree, e);
			}
		}else{
			if(e != null){
				s.setValeur(e.getValeur());
			}
		}
	}
	
	public int getDeplacement(Entree entree) throws PasDeDeclarationException{
		Symbole s = tds.get(entree);
		this.identifier(entree,entree.getLigne());
		return s.getDepl();
	}
}
