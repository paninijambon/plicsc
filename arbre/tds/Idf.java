package plic.arbre.tds;

import plic.arbre.tds.Symbole;

public class Idf {

    private String nom;
    private int noligne;


    public Idf(String nom, int noligne) {
        this.nom = nom;
        this.noligne = noligne;
    }


    public void verifier() {
        Tds.getInstance().identifier(new EntreeVar(nom), noligne);
    }


    public int getDeplacement() {
        return Tds.getInstance().identifier(new EntreeVar(nom), noligne).getDepl();
    }

    public Symbole getSymbole() {
        return Tds.getInstance().identifier(new EntreeVar(nom), noligne);
    }

    public String getNom() {
        return nom;
    }

    public String toString() {
        return nom;
    }

}
