package plic.arbre;

import java.util.ArrayList;

import plic.arbre.tds.Tds;
import plic.exceptions.SemantiqueException;


public class BlocDInstructions extends ArbreAbstrait {

	protected ArrayList<ArbreAbstrait> al;

	public BlocDInstructions() {
		super();
		al = new ArrayList<ArbreAbstrait>();
		
		
	}

	public void ajouter(ArbreAbstrait a) {
		al.add(a);
	}

	@Override
	public String toString() {
		String str = "";
		for (ArbreAbstrait a : al) {
			if (a != null) {
				str += a.toString();
				str += "\n";
			}
		}
		return str;
	}

	@Override
	public void verifier() throws SemantiqueException {
		for (ArbreAbstrait a : al) {
			if (a != null)
				a.verifier();
		}
	}

	@Override
	public String toMips() {
		StringBuilder sb = new StringBuilder();
		
			sb.append(".text \n");
			sb.append("main : \n");

			sb.append("# zone de reservation de memoire\n\n");
			sb.append("	# initialise s7 avec sp \n");
			sb.append("	move $s7,$sp \n");
			sb.append("	# reserve les zones memoire des variables \n");
			for (int i = 0; i < Tds.getInstance().getTds().size(); i++) {
				sb.append("	addi $sp ,$sp,-4 \n");
			}
			sb.append("\n# zone programme\n");
		
		for (ArbreAbstrait a : al) {
			if (a != null)
				sb.append(a.toMips());
		}sb.append("end : \n");
		sb.append("#fin du programme \n");
		sb.append("move $v1,$v0 #copie de v0 dans v1 pour permettre les test de plic 0 \n");
		sb.append("li $v0,10 #retour au systeme \n");
		sb.append("syscall \n");
		String str = sb.toString();
		return str;



	}

}
