package plic.instructions;

import plic.arbre.ArbreAbstrait;
import plic.arbre.BlocDInstructions;
import plic.exceptions.SemantiqueException;

public class EcrireChaine extends BlocDInstructions {

	private String chaine;
	
	public EcrireChaine(String chaine){
		super();
		
	}

	
	
	@Override
	public void verifier() throws SemantiqueException {
	}

	@Override
	public String toMips() {
		StringBuilder ecrire = new StringBuilder(); 
		ecrire.append("	\n# Ecrirechaine\n");
		ecrire.append("	.data \n");
		ecrire.append("	stri"+ArbreAbstrait.cptEtiquette+": .asciiz \"" + chaine +"\"\n");
		ecrire.append("	.text \n");
		ecrire.append("	li $v0, 4 \n");
		ecrire.append("	la $a0, stri"+ ArbreAbstrait.cptEtiquette +"\n");
		ecrire.append("	syscall\n") ;
		incrementeCptEtiquette();
		return ecrire.toString() ;
	}

	@Override
	public String toString() {
		return "Ecriture de la chaine: "+chaine;
	}

}
