package plic.instructions;

import plic.arbre.BlocDInstructions;
import plic.arbre.expression.Expression;
import plic.arbre.tds.EntreeVar;
import plic.arbre.tds.Symbole;
import plic.arbre.tds.SymboleVar;
import plic.arbre.tds.Tds;
import plic.exceptions.SemantiqueException;

public class DeclarationAvecAffectation extends BlocDInstructions {
	
	private String idf, s, t;
	private Expression e ;
	private Affectation affectation;
	
	public DeclarationAvecAffectation(String idf, String s, String t, Expression e, int idfgauche){
		this.idf = idf;
		this.s = s;
		this.t = t;
		this.e = e;
		Symbole symbole = new SymboleVar(s,t);
		symbole.setValeur(e.getValeur());
		Tds.getInstance().ajouter(new EntreeVar(idf, idfgauche), symbole);
		affectation = new Affectation(idf, e, idfgauche);
	}
	
	@Override
	public void verifier() throws SemantiqueException {
		affectation.verifier();
	}

	@Override
	public String toString(){
		return "DeclarationAffectation d'un "+t+" "+s+" appele "+idf+ " avec valeur "+e;		
	}

	@Override
	public String toMips() {
		return affectation.toMips();
	}

}
