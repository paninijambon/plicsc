package plic.exceptions;

public class NonConcordance extends SemantiqueException{
	
	private static final long serialVersionUID = 1L;

	public NonConcordance(String message, int l) {
		super(message,l);
	}
	
}
